var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Popups', function() {
	var _path = 'api/v1/Popups';
	var _parentEntityPath = 'api/v1/Branches';

	var _accountType = 'Admin';
	var _username = 'admin2@mystate.cool';
	var _marketeerId = globals.emptyGuid;
	var _testEntityName = 'Admin:1-Popup:1-Test:1';

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {				
				globals.request({
					url: globals.rootUrl + _path,
					method: "POST",
					body: {
						"Address": null
					}
				})
				.then(
					function(result) {						
						var res = result.response;
						var body = result.body;
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);
						done();
					},
					function(err) {
						console.log('Error', err);
						done(err);
					})
				.catch(function(err){
					console.log('Catch Error',err);
				})
				;
			});
		});
		describe('Authorized', function() {
			adminData = {};
			before(function(done) {				
				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;
						done();
					});
			});

			after(function(done) {
				// runs after all tests in this block
				done();
			});

			// it('Post - Create new Popup - wrong values', function(done) {				
			// 	globals.request({
			// 		url: globals.rootUrl + _path,
			// 		method: 'POST',
			// 		headers: {
			// 			'content-type': 'application/json',
			// 			"Authorization" : adminData.accessToken
			// 		},					    
			// 		body: {
			// 			"Address": "FakeAddress"
			// 		}
			// 	})
			// 	.then(function(result) {
			// 		var res = result.response;
			// 		var body = result.body;					

			// 		globals.check(done, function(){
			// 			expect(res.statusCode).to.equal(400);
			// 			expect(body).to.not.equal(undefined);

			// 			expect(body.ModelState).to.have.property('model.Localizations');
			// 			expect(body.ModelState).to.have.property('model.BranchId')
			// 			.that.include.members([
			// 				'Branch 00000000-0000-0000-0000-000000000000 does not exist.',
			// 				'Branch does not exist.']);
						
			// 		});

			// 	}, function(err) {
			// 		console.log("Error:", err);
			// 		done(err);
			// 	}).catch(function(error){
			// 		assert.isNotOk(error,'Promise error');
			// 	});
			// });

			//Todo: at some point we need to add a cleen-up before and/or after the tests.
			it('Post - Create new Popup', function(done) {
				var testEntityId;
				var testParentEntityId;
				globals.request({
					url: globals.rootUrl + _parentEntityPath,
					method: "GET",
					headers : {
						"Authorization" : adminData.accessToken
					}
				})
				.then(function(result) {
					testParentEntityId = result.body[0].id;

console.log('Sent body',{
							"PhoneNumbers": ["+972545555555"],
							"Type": "REGULAR",
							"Name": _testEntityName,
							"OperatingHours": {
								"WeekDays": [{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								}
								]
							},
							"Images": [{
								"Name": "COVER",
								"Url": "https://businesspopupdev.blob.core.windows.net/68b6567b-548f-4c61-a0de-c223518f2b60/cover_7f7ea3ea-b339-4473-b891-57460b86ba85",
								"id": "/68b6567b-548f-4c61-a0de-c223518f2b60/cover_7f7ea3ea-b339-4473-b891-57460b86ba85",
								"Size": 86295
							}],
							"Address": null,
							"Geolocation": {
								"Lat": 0,
								"Lon": 0
							},
							"Timezone": "Asia/Jerusalem",
							"Banner": {
								"LinkUrl": "",
								"Images": null
							},
							"Properties": [],
							"Actions": [{
								"ActionType": "LINK",
								"Name": "Action 1",
								"Icon": "",
								"Data": "",
								"Order": 1,
								"ShowWhenOpen": true,
								"ShowWhenClosed": true
							}],
							"Localizations": {
								"he-IL": {
									"Direction": "rtl",
									"Data": {
										"FIELD1": "טקסט 1",
										"FIELD2": "טקסט 2",
										"FIELD3": "טקסט 3",
										"FIELD4": "טקסט 4",
										"FIELD5": "טקסט 5"
									}
								}
							},
							"IsActive": false,
							"BranchId": testParentEntityId
						});

					globals.request({
						url: globals.rootUrl + _path,
						method: 'POST',
						headers: {
							'content-type': 'application/json',
							"Authorization" : adminData.accessToken
						},					    
						body:     {
							"PhoneNumbers": ["+972545555555"],
							"Type": "REGULAR",
							"Name": _testEntityName,
							"OperatingHours": {
								"WeekDays": [{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								},
								{
									"ActivityHours": []
								}
								]
							},
							"Images": [{
								"Name": "COVER",
								"Url": "https://businesspopupdev.blob.core.windows.net/68b6567b-548f-4c61-a0de-c223518f2b60/cover_7f7ea3ea-b339-4473-b891-57460b86ba85",
								"id": "/68b6567b-548f-4c61-a0de-c223518f2b60/cover_7f7ea3ea-b339-4473-b891-57460b86ba85",
								"Size": 86295
							}],
							"Address": null,
							"Geolocation": {
								"Lat": 0,
								"Lon": 0
							},
							"Timezone": "Asia/Jerusalem",
							"Banner": {
								"LinkUrl": "",
								"Images": null
							},
							"Properties": [],
							"Actions": [{
								"ActionType": "LINK",
								"Name": "Action 1",
								"Icon": "",
								"Data": "",
								"Order": 1,
								"ShowWhenOpen": true,
								"ShowWhenClosed": true
							}],
							"Localizations": {
								"he-IL": {
									"Direction": "rtl",
									"Data": {
										"FIELD1": "טקסט 1",
										"FIELD2": "טקסט 2",
										"FIELD3": "טקסט 3",
										"FIELD4": "טקסט 4",
										"FIELD5": "טקסט 5"
									}
								}
							},
							"IsActive": false,
							"BranchId": testParentEntityId
						}
					})
					.then(function(result) {
						var res = result.response;
						var body = result.body;

						console.log('body', body);

						expect(res.statusCode).to.equal(201);
						expect(body).to.not.equal(undefined);
						expect(body).to.not.be.empty;

						testEntityId = body.id;

					}, function(err) {
						console.log("Error:", err);
						done(err);
					})
					.then(function(result) {

						globals.request({
							url: globals.rootUrl + _path + '/' + testEntityId,
							method: 'DELETE',
							headers: {
								'content-type': 'application/json',
								"Authorization" : adminData.accessToken
							}
						})
						.then(function(response){						
							done();
						},
						function(err){
							console.log('error', err);
							done(err);
						})
						.catch(function(error){
							assert.isNotOk(error,'Promise error');
						});

					}, function(err) {
						console.log("Error:", err);
						done(err);
					})
					.catch(function(error){
						assert.isNotOk(error,'Promise error');
					});
				}, function(err) {
					console.log("Error:", err);
					done();
				})				
			});
		});
});
});
