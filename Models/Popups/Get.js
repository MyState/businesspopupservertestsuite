var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Popups', function() {
	var _path = 'api/v1/Popups';

	var _accountType = 'Admin';
	var _username = 'admin2@mystate.cool';

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {				
				globals.request({
					url: globals.rootUrl + _path,
					method: "GET"
				})
				.then(
					function(result) {
						var res = result.response;
						var body = result.body;						
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);
						done();
					},
					function(err) {
						console.log(err);
						done(err);
					}
				);
			});
		});
		describe('Authorized', function() {
			adminData = {};
			before(function(done) {			

				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;						
						done();
					}
				);
			});

			after(function(done) {
				// runs after all tests in this block				
				done();
			});
			it('Get: should return list of Popups', function(done) {
				globals.request({
					url: globals.rootUrl + _path,
					method: "GET",
					headers : {
						"Authorization" : adminData.accessToken
					}
				})
				.then(function(result) {

					var res = result.response;
					var body = result.body;
					globals.check(done, function(){
						expect(res.statusCode).to.equal(200);
						expect(body).to.not.equal(undefined);
						expect(body).to.be.not.empty;
						expect(body).to.have.deep.property('[0].Name');
					});				
				}, function(err) {
					console.log("Error:", err);
					done();
				});
			});

			it('Get - with wrong Popup Id: should return empty list', function(done) {
				var queryString = globals.getQueryString('Id','=', globals.emptyGuid);
				globals.request({
					url: globals.rootUrl + _path + queryString,
					method: "GET",
					headers : {
						"Authorization" : adminData.accessToken
					}
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;
					globals.check(done, function(){
						expect(res.statusCode).to.equal(200);						
						expect(body).to.not.equal(undefined);
						expect(body).to.be.empty;
					});					
				}, function(err) {
					console.log("Error:", err);
					done();
				});
			});
		});
	});
});
