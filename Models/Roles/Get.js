var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');

var admin1AccessToken = "";

describe('Roles', function() {
	var _path = 'api/v1/Roles';
	describe('Get', function() {
		it('should return all possible roles', function(done) {
			var expectedRolesList = [{
				Name: 'Admin',
				BusinessEntityName: 'MyState',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'MarketeerManager',
				BusinessEntityName: 'MyState',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'FinanceManager',
				BusinessEntityName: 'MyState',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'AnalyticsManager',
				BusinessEntityName: 'MyState',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'Admin',
				BusinessEntityName: 'Marketeer',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'AccountManager',
				BusinessEntityName: 'Marketeer',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'BranchesManager',
				BusinessEntityName: 'Marketeer',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'FinanceManager',
				BusinessEntityName: 'Marketeer',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'Admin',
				BusinessEntityName: 'Business',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'BranchManager',
				BusinessEntityName: 'Business',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'FinanceManager',
				BusinessEntityName: 'Business',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'Publisher',
				BusinessEntityName: 'Business',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'Editor',
				BusinessEntityName: 'Business',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}, {
				Name: 'Admin',
				BusinessEntityName: 'Branch',
				BusinessEntityId: '00000000-0000-0000-0000-000000000000'
			}];
			globals.request({
				url: globals.rootUrl + _path,
				method: "GET"
			})
			.then(function(result) {
				var res = result.response;
				var body = result.body;
				expect(res.statusCode).to.equal(200);
				var flatRoles = _.map(body, function(e) { return e.Name + ":" + e.BusinessEntityName; });
				var flatExpected = _.map(expectedRolesList, function(e) { return e.Name + ":" + e.BusinessEntityName; });
				var cmp1 = _.difference(flatRoles, flatExpected);
				var cmp2 = _.difference(flatExpected, flatRoles);
				expect(cmp1.length).to.equal(0);
				expect(cmp2.length).to.equal(0);
				done();
			}, function(err) {
				expect(err).to.equal(null);
				console.log("Error:", err);
				done();
			});
		});
	});
});
