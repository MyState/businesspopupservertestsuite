var Mocha = require("mocha");
var Reporter = require("reporter");

var mocha = new Mocha({
	ui: "tdd",
	reporter: "spec"
});
mocha.addFile("Models/Accounts/Get.js");
mocha.addFile("Models/Branches/Get.js");
mocha.addFile("Models/Businesses/Get.js");
mocha.addFile("Models/Classifications/Get.js");
mocha.addFile("Models/CommunicationChannels/Get.js");
mocha.addFile("Models/Marketeers/Get.js");
mocha.addFile("Models/Permissions/Get.js");
mocha.addFile("Models/Popups/Get.js");
mocha.addFile("Models/Roles/Get.js");

mocha.run();
