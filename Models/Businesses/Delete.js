var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Businesses', function() {
	var _path = 'api/v1/Businesses';

	var _accountType = 'Admin';
	var _username = 'admin2@mystate.cool'; //globals.getUserName('AccountManager', 1);	
	var _marketeerId = globals.emptyGuid;	

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {
				globals.request({
					url: globals.rootUrl + _path + '/' + globals.emptyGuid,
					method: "Delete",
					headers: {
			            'content-type': 'application/json'
				    }
				})
				.then(
					function(result) {
						var res = result.response;
						var body = result.body;
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);						
						done();
					},
					function(err) {
						console.log(err);
						done(err);
					}
				).catch(function(err){
					console.log(err);
					done(err);
				});
			});
		});
		describe('Authorized', function() {
			var adminData = {};
			before(function(done) {
				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;						

						globals.request({						
					      	url: globals.rootUrl + _path,
					      	method: 'POST',
						    headers: {
					            'content-type': 'application/json',
								"Authorization" : adminData.accessToken
						    },					    
					      	body: {
							    "Name": "Admin:1-Business:1-Put-Test:1",
						        "Images": [],
							    "IsActive": false,
							    "MarketeerId": _marketeerId
							}
						})
						.then(function(response){
							adminData.testBusinessId = response.body.id;
							done();
						});
					}
				);
			});

			after(function(done) {
				done();
			});
				
			it('Delete' ,function(done) {
					globals.request({
			      	url: globals.rootUrl + _path + '/' + adminData.testBusinessId,
			      	method: 'DELETE',
				    headers: {
			            'content-type': 'application/json',
						"Authorization" : adminData.accessToken
				    }
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					globals.check(done, function(){
						expect(res.statusCode).to.equal(204);
						expect(body).to.equal(undefined);						
					});

				}, function(err) {
					console.log("Error:", err);
					done(err);
				}).catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});
		});
	});
});
