var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');

var admin1AccessToken = "";

describe('Permissions', function() {
	var _path = 'api/v1/Permissions';
	describe('Get', function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {
				globals.request({
					url: globals.rootUrl + _path,
					method: "GET"
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;
					expect(body).to.equal(undefined);
					expect(res.statusCode).to.equal(401);
					done();
				}, function(err) {
					console.log(err);
					done();
				});
			});
		});
		describe('Authorized', function() {
			describe('MyState Admin', function() {
				adminData = {};
				before(function(done) {
					globals.login("admin1")
					.then(function(result) {
						var res = result.response;
						var body = result.body;
						adminData.accessToken = body.token_type + " " + body.access_token;
						done();
					},
					function(err) {
						expect(err).to.equal(null);
						done();
					});
				});

				after(function(done) {
					// runs after all tests in this block
					done();
				});
				it('should return permissions list', function(done) {
					globals.request({
						url: globals.rootUrl + _path,
						method: "GET",
						headers : {
							"Authorization" : adminData.accessToken
						}
					})
					.then(function(result) {
						var res = result.response;
						var body = result.body;
						expect(res.statusCode).to.equal(200);
						var adminsPermission = _.map(body, function(e) {
							if (e.Name === 'Admin' && e.BusinessEntityName === 'MyState')
								return e;
						});
						expect(adminsPermission).to.have.length.above(1);
						done();
					}, function(err) {
						console.log("Error:", err);
						done();
					});
				});
			});
		});
	});
});
