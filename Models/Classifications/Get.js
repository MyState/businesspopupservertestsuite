var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');

describe('Classifications', function() {
	var _path = 'api/v1/Classifications';
	describe('Get', function() {
		it('should return not implemented error', function(done) {
			globals.request({
				url: globals.rootUrl + _path,
				method: "GET"
			})
			.then(function(result) {
				var res = result.response;
				var body = result.body;
				expect(res.statusCode).to.equal(500);
				expect(body.ExceptionType).to.equal("System.NotImplementedException");
				expect(body.ExceptionMessage).to.equal("The method or operation is not implemented.");
				done();
			}, function(err) {
				console.log(err);
				done();
			});
		});
	});
});
