var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');

describe('Accounts', function() {
	var _path = 'api/v1/accounts/Users';
	describe('Get', function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {
				globals.request({
					url: globals.rootUrl + _path,
					method: "GET",
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;
					expect(res.statusCode).to.equal(401);
					expect(body.Message).to.equal("Authorization has been denied for this request.");
					done();
				}, function(err) {
					console.log("Error:", err);
					done();
				});
			});
		});
		describe('Authorized', function() {
			describe('MyState Admin', function() {
				adminData = {};
				before(function(done) {
					globals.login("admin1")
					.then(
						function(result) {
							adminData.accessToken = result.body.token_type + " " + result.body.access_token;
							done();
						},
						function(err) {
							done();
						}
					);
				});

				after(function(done) {
					// runs after all tests in this block
					done();
				});
				it('should return two admins', function(done) {
					globals.request({
						url: globals.rootUrl + _path,
						method: "GET",
						headers : {
							"Authorization" : adminData.accessToken
						}
					})
					.then(function(result) {
						var res = result.response;
						var body = result.body;
						expect(res.statusCode).to.equal(200);
						var admin1 = _.filter(body, function(e) { if (e.UserName === "admin1") return e; })[0];
						var admin2 = _.filter(body, function(e) { if (e.UserName === "admin2") return e; })[0];
						expect(admin1.UserName).to.equal("admin1");
						expect(admin2.UserName).to.equal("admin2");
						done();
					}, function(err) {
						console.log("Error:", err);
						done();
					});
				});
			});
		});
	});
});
