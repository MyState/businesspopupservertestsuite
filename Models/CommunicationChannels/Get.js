var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');

describe('CommunicationChannels', function() {
	var _path = 'api/v1/CommunicationChannels';
	describe('Get', function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {
				globals.request({
					url: globals.rootUrl + _path,
					method: "GET"
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;
					expect(body).to.equal(undefined);
					expect(res.statusCode).to.equal(401);
					done();
				}, function(err) {
					console.log(err);
					done();
				});
			});
		});
	});
});
