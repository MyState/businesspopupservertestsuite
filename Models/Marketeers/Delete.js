var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Marketeers', function() {
	var _path = 'api/v1/Marketeers';

	var _accountType = 'Admin';
	var _username = 'admin2@mystate.cool';
	var _marketeerId = globals.emptyGuid;
	var _testEntityName = 'Admin:1-Marketeer:1-Test:1';

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {
				globals.request({
					url: globals.rootUrl + _path + '/' + globals.emptyGuid,
					method: "Delete",
					headers: {
			            'content-type': 'application/json'
				    }
				})
				.then(
					function(result) {
						var res = result.response;
						var body = result.body;
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);						
						done();
					},
					function(err) {
						console.log(err);
						done(err);
					}
				).catch(function(err){
					console.log(err);
					done(err);
				});
			});
		});
		describe('Authorized', function() {
			var adminData = {};
			before(function(done) {
				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;						

						globals.request({						
					      	url: globals.rootUrl + _path,
					      	method: 'POST',
						    headers: {
					            'content-type': 'application/json',
								"Authorization" : adminData.accessToken
						    },					    
					      	body: {
							    "Name": _testEntityName,
						        "Images": [],
							    "IsActive": false							    
							}
						})
						.then(function(response){
							adminData.testEntityId = response.body.id;
							done();
						});
					}
				);
			});

			after(function(done) {
				done();
			});
				
			it('Delete' ,function(done) {
					globals.request({
			      	url: globals.rootUrl + _path + '/' + adminData.testEntityId,
			      	method: 'DELETE',
				    headers: {
			            'content-type': 'application/json',
						"Authorization" : adminData.accessToken
				    }
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					globals.check(done, function(){
						expect(res.statusCode).to.equal(204);
						expect(body).to.equal(undefined);						
					});

				}, function(err) {
					console.log("Error:", err);
					done(err);
				}).catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});
		});
	});
});
