var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Marketeers', function() {
	var _path = 'api/v1/Marketeers';

	var _accountType = 'Admin';
	var _username = 'admin2@mystate.cool';
	var _marketeerId = globals.emptyGuid;
	var _testEntityName = 'Admin:1-Marketeer:1-Test:1-updated';	

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {
				globals.request({
					url: globals.rootUrl + _path + '/' + globals.emptyGuid,
					method: "PUT",
					headers: {
			            'content-type': 'application/json'
				    },					    
			      	body: {					    
			      		"id": globals.emptyGuid,
					    "Name": _testEntityName,
				        "Images": [],
					    "IsActive": false
					}
				})
				.then(
					function(result) {
						var res = result.response;
						var body = result.body;
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);						
						done();
					},
					function(err) {
						console.log(err);
						done(err);
					}
				).catch(function(err){
					console.log(err);
					done(err);
				});
			});
		});
		describe('Authorized', function() {
			var adminData = {};
			before(function(done) {
				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;						

						globals.request({						
					      	url: globals.rootUrl + _path,
					      	method: 'POST',
						    headers: {
					            'content-type': 'application/json',
								"Authorization" : adminData.accessToken
						    },					    
					      	body: {
							    "Name": "Admin:1-Marketeer:1-Put-Test:1",
						        "Images": [],
							    "IsActive": false
							}
						})
						.then(function(response){
							adminData.testEntityId = response.body.id;
							done();
						});
					}
				);
			});

			after(function(done) {				
				globals.request({
				      	url: globals.rootUrl + _path + '/' + adminData.testEntityId,
				      	method: 'DELETE',
					    headers: {
				            'content-type': 'application/json',
							"Authorization" : adminData.accessToken
					    }
					})
					.then(function(response){						
						done();
					},
					function(err){
						console.log('error', err);
						done(err);
					})
					.catch(function(error){
						assert.isNotOk(error,'Promise error');
					});								
			});
				
			it('Put - Update Marketeer - wrong values' ,function(done) {
				globals.request({
					url: globals.rootUrl + _path + '/' + adminData.testEntityId,
					method: 'PUT',
				    headers: {
			            'content-type': 'application/json',
						"Authorization" : adminData.accessToken
				    },					    
			      	body: {					    
			      		"id": adminData.testEntityId,
			      		"Images": [{
			      			Url: "NotUrl",
			      			Name: "NotCOVER_Or_LOGO"
			      		}]
			      	}
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					globals.check(done, function(){
						expect(res.statusCode).to.equal(400);
						expect(body).to.not.equal(undefined);
						expect(body.ModelState.model[0]).to.contain('IsActive');
						expect(body.ModelState).to.have.property('model.Name');
						expect(body.ModelState).to.have.property('model.Images[0].Url').that.is.an('array');
					});

				}, function(err) {
					console.log("Error:", err);
					done(err);
				}).catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});

			it('Put - Update Marketeer', function(done) {
				globals.request({						
			      	url: globals.rootUrl + _path + '/' + adminData.testEntityId,
			      	method: 'PUT',
				    headers: {
			            'content-type': 'application/json',
						"Authorization" : adminData.accessToken
				    },					    
			      	body: {
					    "id": adminData.testEntityId,
					    "Name": "Admin:1-Business:1-Put-Test:1-updated",
				        "Images": [],
					    "IsActive": false
					}
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					globals.check(done, function(){
						expect(res.statusCode).to.equal(204);
						expect(body).to.equal(undefined);						
					});					
					
				}, function(err) {
					console.log("Error:", err);
					done(err);
				}).catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});			
		});
	});
});
