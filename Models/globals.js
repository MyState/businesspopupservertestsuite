var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var Promise = require('promise');


// var ROOT_URL = "https://business-popup-dev.azurewebsites.net/";
var ROOT_URL = "http://localhost:43380/";

var PASSWORD = "123456aA!";

var debug = function(){};
//var debug = function() { console.log(arguments); }

module.exports = {	
	rootUrl: ROOT_URL,
	emptyGuid: '00000000-0000-0000-0000-000000000000',
	request: function(options) 	{
		options = options || {};
		options.json = options.json || true;

		return new Promise(function (resolve, reject) {
			// console.log('request -> parameters: ');
			// console.log(options	);

			request(options, function (err, res, bodyData) {
				if (err) {
					console.log('err');
					return reject(err);
				}				
				// console.log('Response: ');
				//console.log(res);
				//console.log("res.body", res.body, res.statusCode, typeof(res.body));				

				if (res.statusCode !== 200 && res.statusCode !== 201) {
					var body = module.exports.tryParseJson(res.body);					
					return resolve({
						body: body,
						response: res
					});
				}

				resolve({								
					body: module.exports.tryParseJson(bodyData),
					response: res
				});
			});
		});
	},
	check : function( done, f ) {
		var message;
		try {
			f();			
		}
		catch(e) {
			message = e;	
		}
		
		done(message);		
	},

	login: function(username) {
		var self = this;
		debug("login("+username+")");
		return self.request({
			url: self.rootUrl + 'oauth/token',
			method: "POST",
			form: {
				grant_type: "password",
				username: username,
				password: PASSWORD
			}
		});
	},
	getUserDetails: function(username) {
		var self = this;
		debug("getUserDetails("+username+")");
		return self.login("admin1")
		.then(function(result) {
			var body = result.body;
			accessToken = body.token_type + " " + body.access_token;
			return self.request({
				url: self.rootUrl + 'api/v1/accounts/User/' + username,
				method: "GET",
				headers: {
					"Authorization": accessToken
				}
			});
		});
	},
	getUserDetailsByToken: function(token){		
		self.request({
			url: self.rootUrl + 'api/v1/accounts/User/' + username,
			method: "GET",
			headers: {
				"Authorization": token
			}
		});
	},
	createMarketeerWithUser: function(username, marketeer) {
		var self = this;
		var adminAccessToken;
		debug("createMarketeerWithUser("+username+")");
		return self.login("admin1")
		.then(function(result) {
			var body = result.body;
			adminAccessToken = body.token_type + " " + body.access_token;
			debug("createMarketeerWithUser -> ", body);
			return self.request({
				url: self.rootUrl + 'api/v1/Marketeers',
				method: "POST",
				headers : {
					"content-type": "application/json",
					"Authorization" : adminAccessToken
				},
				body: JSON.stringify(marketeer)
			});
		});
	},
	giveUserPermission: function(username, role) {
		var self = this;
		var theUser;
		var adminAccessToken;
		debug("giveUserPermission("+username+","+role+")");
		return self.login("admin1")
		.then(function(result) {
			var body = result.body;
			adminAccessToken = body.token_type + " " + body.access_token;
			debug("giveUserPermission->body=", body);
			return self.getUserDetails(username);
		})
		.then(function(result) {
			theUser = result.body;
			debug("giveUserPermission->theUser=", theUser);
			return self.request({
				url: self.rootUrl + 'api/v1/Permissions',
				method: "POST",
				headers : {
					"content-type": "application/json",
					"Authorization" : adminAccessToken
				},
				body: JSON.stringify({
					UserId: theUser.Id,
					Role: {
						Name: role.Name,
						BusinessEntityName: role.BusinessEntityName,
						BusinessEntityId: role.BusinessEntityId
					}	
				})
			});
		});
	},
	cleanup: function(cleanThese) {
		var self = this;
		var deletePromisses = [];
		debug("cleanup", cleanThese);
		var adminAccessToken;
		return self.login("admin1")
		.then(function(result) {
			var body = result.body;
			adminAccessToken = body.token_type + " " + body.access_token;
			_.map(cleanThese, function(e) {
				var thePromise = self.request({
					url: self.rootUrl + 'api/v1/' + e.type + "/" + e.id,
					method: "DELETE",
					headers : {
						"Authorization" : adminAccessToken
					}
				});
				deletePromisses.push(thePromise);
			});
			return Promise.all(deletePromisses);
		});
	},
	getUserName: function(role, marketeerNum, businessNum, branchNum) {		
		debug("getUserName(" + marketeerNum + ', ' + businessNum + ', ' + branchNum + ")");
		
		var username = 'tester0m' + marketeerNum || '';
		if (businessNum !== undefined){
			username +=  'b' + businessNum;
		}
		if (branchNum !== undefined){
			username += 'b' + branchNum;
		}

		username += role + '@mystate.cool';

		return username;
	},
	getQueryString: function(propertyName, condition, value) {
		//?where=filter(Id%2C%3D%2C00000000-0000-0000-0000-000000000000)
		var filter = encodeURIComponent(propertyName+','+condition+','+value);
		var queryString = '?where=filter('+filter+')';
		return queryString;
	},
	tryParseJson: function  (source){
		if (Object.prototype.toString.call(source) === "[object String]")
		{
			var body;
			try { 
				body = JSON.parse(res.body)
			} catch (e) {
				body = undefined;
			}	
		}
		else
		{
			return source;
		}
	},
	expectArray: function(source, values, comperator, errMessage, valuesFormat){
		source.forEach(function(x){
			var isMatch = false;
			for (var i = values.length - 1; i >= 0; i--) {
				var value = valuesFormat === undefined ? 
				values[i] :
				String.format(valuesFormat, values[i]);
				if (comperator(x, value))
					isMatch = true;
			}
			expect(isMatch).to.equal(true, String.format(errMessage, x));
		});
	},
	requiredPrpertyFormat: 'Required property \'{0}\' not found in JSON. Path \'\'',
	requiredPrpertyErrorFormat : 'Expected property: {0} was not found'
};

if (!String.format) {
	String.format = function(format) {
		var args = Array.prototype.slice.call(arguments, 1);
		return format.replace(/{(\d+)}/g, function(match, number) { 
			return typeof args[number] != 'undefined'
			? args[number] 
			: match
			;
		});
	};
}