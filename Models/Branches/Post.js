var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Branches', function() {
	var _path = 'api/v1/Branches';
	var _businessesPath = 'api/v1/Businesses';

	var _accountType = 'Admin';
	var _username = 'admin2@mystate.cool';
	var _marketeerId = globals.emptyGuid;
	var _testEntityName = 'Admin:1-Branch:1-Test:1';

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {				
				globals.request({
					url: globals.rootUrl + _path,
					method: "POST",
					body: {
						"Address": "FakeAddress"
					}
				})
				.then(
					function(result) {						
						var res = result.response;
						var body = result.body;
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);
						done();
					},
					function(err) {
						console.log('Error', err);
						done(err);
					}
					)
				.catch(function(err){
					console.log('Catch Error',err);
				})
				;
			});
		});
		describe('Authorized', function() {
			adminData = {};
			before(function(done) {				
				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;
						done();
					});
			});

			after(function(done) {
				// runs after all tests in this block
				done();
			});

			it('Post - Create new Branch - wrong values', function(done) {				
				globals.request({
					url: globals.rootUrl + _path,
					method: 'POST',
					headers: {
						'content-type': 'application/json',
						"Authorization" : adminData.accessToken
					},					    
					body: {
						"Address": "FakeAddress"
					}						    
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					var expectedMissingProperties = [ 'BusinessId', 'IsVirtual', 'IsActive' ];
					
					globals.check(done, function(){
						expect(res.statusCode).to.equal(400);
						expect(body).to.not.equal(undefined);

						globals.expectArray(body.ModelState.model, 
							expectedMissingProperties, 
							function(x,y){ return x.includes(y);},
							globals.requiredPrpertyErrorFormat, 
							globals.requiredPrpertyFormat);
						
						expect(body.ModelState).to.have.property('model.Name');
						expect(body.ModelState).to.have.property('model.Description');
						expect(body.ModelState).to.have.property('model.Timezone');
					});

				}, function(err) {
					console.log("Error:", err);
					done(err);
				}).catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});

			//Todo: at some point we need to add a cleen-up before and/or after the tests.
			it('Post - Create new Branch', function(done) {
				var testEntityId;
				var testParentEntityId;
				globals.request({
					url: globals.rootUrl + _businessesPath,
					method: "GET",
					headers : {
						"Authorization" : adminData.accessToken
					}
				})
				.then(function(result) {
					testParentEntityId = result.body[0].id;

					globals.request({
						url: globals.rootUrl + _path,
						method: 'POST',
						headers: {
							'content-type': 'application/json',
							"Authorization" : adminData.accessToken
						},					    
						body: {
							"BusinessId": testParentEntityId,
							"IsVirtual": true,
							"Name": _testEntityName,
							"Description": "string",
							"Timezone": "IL",
							"Address": "tel aviv",
							"GeoLocation": {
								"Lat": 0,
								"Lon": 0
							},
							"ClassificationIds": null,
							"LookAndFeel": {},
							"IsActive": true    
						}
					})
					.then(function(result) {
						var res = result.response;
						var body = result.body;

						expect(res.statusCode).to.equal(201);
						expect(body).to.not.equal(undefined);
						expect(body).to.not.be.empty;

						testEntityId = body.id;

					}, function(err) {
						console.log("Error:", err);
						done(err);
					})
					.then(function(result) {

						globals.request({
							url: globals.rootUrl + _path + '/' + testEntityId,
							method: 'DELETE',
							headers: {
								'content-type': 'application/json',
								"Authorization" : adminData.accessToken
							}
						})
						.then(function(response){						
							done();
						},
						function(err){
							console.log('error', err);
							done(err);
						})
						.catch(function(error){
							assert.isNotOk(error,'Promise error');
						});

					}, function(err) {
						console.log("Error:", err);
						done(err);
					})
					.catch(function(error){
						assert.isNotOk(error,'Promise error');
					});
				}, function(err) {
					console.log("Error:", err);
					done();
				})				
			});
		});
});
});
