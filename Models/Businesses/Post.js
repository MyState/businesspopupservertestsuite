var request = require('request');
var chai = require('chai');
var expect = chai.expect;
var _ = require('underscore');
var globals = require('../globals');


describe('Businesses', function() {
	var _path = 'api/v1/Businesses';

	var _accountType = 'Admin2';
	var _username = 'admin2@mystate.cool'; //globals.getUserName('AccountManager', 1);	
	var _marketeerId = globals.emptyGuid;

	describe(_accountType, function() {
		describe('Unauthorized', function() {
			it('should return unauthorized error', function(done) {				
				globals.request({
					url: globals.rootUrl + _path,
					method: "POST"
				})
				.then(
					function(result) {
						var res = result.response;
						var body = result.body;
						expect(body).to.equal(undefined);
						expect(res.statusCode).to.equal(401);
						done();
					},
					function(err) {
						console.log(err);
						done(err);
					}
				);
			});
		});
		describe('Authorized', function() {
			adminData = {};
			before(function(done) {				
				globals.login(_username)
				.then(
					function(result) {
						adminData.accessToken = result.body.token_type + " " + result.body.access_token;
						done();
					}
				);
			});

			after(function(done) {
				// runs after all tests in this block
				done();
			});

			it('Post - Create new Business - wrong values', function(done) {					
				globals.request({						
			      	url: globals.rootUrl + _path,
			      	method: 'POST',
				    headers: {
			            'content-type': 'application/json',
						"Authorization" : adminData.accessToken
				    },					    
			      	body: {
			      		"Images": [{
			      			Url: "NotUrl",
			      			Name: "NotCOVER_Or_LOGO"
			      		}]
			      	}						    
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					globals.check(done, function(){
						expect(res.statusCode).to.equal(400);
						expect(body).to.not.equal(undefined);							
						expect(body.ModelState.model[0]).to.contain('IsActive');
						expect(body.ModelState.model[1]).to.contain('MarketeerId');
						expect(body.ModelState).to.have.property('model.Name');
						expect(body.ModelState).to.have.property('model.Images[0].Url').that.is.an('array');
					});

				}, function(err) {
					console.log("Error:", err);
					done(err);
				}).catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});
			
			//Todo: at some point we need to add a cleen-up before and/or after the tests.
			it('Post - Create new Business', function(done) {
				var newBusinessId;
				globals.request({						
			      	url: globals.rootUrl + _path,
			      	method: 'POST',
				    headers: {
			            'content-type': 'application/json',
						"Authorization" : adminData.accessToken
				    },					    
			      	body: {
					    "Name": "Admin:1-Business:1-Test:1",
				        "Images": [],
					    "IsActive": false,
					    "MarketeerId": _marketeerId
					}
				})
				.then(function(result) {
					var res = result.response;
					var body = result.body;

					expect(res.statusCode).to.equal(201);
					expect(body).to.not.equal(undefined);
					expect(body).to.not.be.empty;

					newBusinessId = body.id;
					
				}, function(err) {
					console.log("Error:", err);
					done(err);
				})
				.then(function(result) {
					
					globals.request({
				      	url: globals.rootUrl + _path + '/' + newBusinessId,
				      	method: 'DELETE',
					    headers: {
				            'content-type': 'application/json',
							"Authorization" : adminData.accessToken
					    }
					})
					.then(function(response){						
						done();
					},
					function(err){
						console.log('error', err);
						done(err);
					})
					.catch(function(error){
						assert.isNotOk(error,'Promise error');
					});
					
				}, function(err) {
					console.log("Error:", err);
					done(err);
				})
				.catch(function(error){
					assert.isNotOk(error,'Promise error');
				});
			});
		});
	});
});
